/* ˳��ջ��ʵ�� 
	ջ��ָ��ָ�����ջ��Ԫ��
*/	


#include <iostream>

#define MaxSize 50
 
struct SqStack {
	int data[MaxSize];
	int top;
	
	SqStack() : top(-1){}  // ˳��ջ�ĳ�ʼ��
	bool StackEmpty(){
		if(top == -1)      // ջ�� 
			return true;
		return false;      
	} 
	bool Push(int x) {  	
		if(top == MaxSize-1)  // ջ��������
			return false;
		data[++top] = x;      // ��ָ�� +1������ջ
		return true; 
	}
	bool Pop(int &x) {
		if(top == -1)       // ջ�գ�����
			return false;
		x=data[top--];      // �ȳ�ջ��ָ���� -1 
		return true; 
	}
	bool GetTop(int &x) {
		if(top == -1)      // ջ�� ������ 
			return false;
		x=data[top];      // x ��¼ջ��Ԫ��
		return true; 
	}
}; 

int main(){
	SqStack s; 
	s.Push(1);
	s.Push(2);
	int x;
	s.Pop(x);
	std::cout << x << std::endl;
	s.Pop(x);
	std::cout << x << std::endl;	
	return 0;
}
